#
# Copyright (C) 2016 The CyanogenMod Project
# Copyright (C) 2017 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit from montana device
$(call inherit-product, device/motorola/jeter/device.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

## Device identifier. This must come after all inclusions
PRODUCT_DEVICE := jeter
PRODUCT_NAME := lineage_moto_g6_play
PRODUCT_BRAND := motorola
PRODUCT_MANUFACTURER := motorola
PRODUCT_MODEL := Moto G6 Play

PRODUCT_ENFORCE_RRO_TARGETS := \
    framework-res

PRODUCT_SYSTEM_PROPERTY_BLACKLIST := ro.product.model

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRODUCT_NAME="Moto G6 play"

# Boot animation
TARGET_SCREEN_WIDTH := 720
TARGET_SCREEN_HEIGHT := 1440

# for specific
$(call inherit-product, vendor/motorola/jeter-vendor.mk)

## Use th latest approved GMS identifiers unless running a signed buil
ifneq ($(SIGN_BULD),true)
PRODUCT_BUILD_PROP_OVERRIDES += \
    BUILD-FINGERPRINT=motorola/aljeter/aljeter:8.0.0/OPPS27.91-143-2/2:user/release-keys
    PRIVATE_BUILD_DESC="aljeter-user 8.0.0 OPPS27.91-143-2 2 release-keys"
endif


#todoooo add fingergprint


